$(document).ready(function() {

  function Apex() {
    this.speechRecognition = new webkitSpeechRecognition();
  }

  Apex.prototype.initialize = function() {
    let that = this;

    this.speechRecognition.continuous = true;
    this.speechRecognition.maxAlternatives = 10;
    this.speechRecognition.interimResults = true;
    this.speechRecognition.lang = 'en-US';

    this.speechRecognition.onresult = function(event) {
      if (event.results[event.results.length - 1].isFinal && event.results[event.results.length - 1][0].confidence > 0.40) 
        that.processCommand(event.results[event.results.length - 1][0].transcript.trim());
    }

    this.speechRecognition.start();

    //this.speech.onend   = function(event) { that.updateIndicator(false); }
    //this.speech.onstart = function(event) { that.updateIndicator(true);  }

    this.speechRecognition.onend = function() { that.speechRecognition.start(); }
  };

  Apex.prototype.processCommand = function(command) {
      $('#current-command').empty();
      $('#current-command').html(command);

    if (command.trim() == 'remove list') {
      $('.v-commands-list').slideUp();
      $('.commands-icon').removeClass('fa-chevron-down');
      $('.commands-icon').addClass('fa-chevron-up');
    }
    else if (command.trim() == 'show list') {
      $('.v-commands-list').slideDown();
      $('.commands-icon').removeClass('fa-chevron-up');
      $('.commands-icon').addClass('fa-chevron-down');
    }
    else if (command.trim().toLowerCase() == 'tell me a quote') {

      let API_LINK_QUOTE_OF_THE_DAY = 'https://quotes.rest/qod.json?category=inspire';

        this.getJSON(API_LINK_QUOTE_OF_THE_DAY, function(err, data) {
            if (data != null)
              $('.bot-result').text('"' + data.contents.quotes[0].quote + '"' + ' - ' +  data.contents.quotes[0].author)

        });
    }
  };

  Apex.prototype.getJSON = function(url, callback)
  {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.responseType = 'json';

      xhr.onload = function() {
          let status = xhr.status;
          
          if (status == 200) {
              callback(null, xhr.response);
          } else {
              callback(status);
          }
      };
      xhr.send();
  };

  let apex = new Apex();
  apex.initialize();
});