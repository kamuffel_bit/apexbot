<!doctype html>
<html lang="en">
 	<head>
 		 <!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		
		<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" rel="stylesheet" >
		<link href="./css/style.css" rel="stylesheet" >

		<title>smart mirror | apexbot</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row p-0 pb-5">
				<div class="col-12 p-0"><img src="./hud/download.svg" width="150px"></div>
			</div>
			<div class="row justify-content-center pt-5 pb-5">
				<div class="col-12 text-center bot-text-display">
					Good evening!
				</div>
		    </div>
		    <div class="row justify-content-center pt-5">
				<div class="col-12 text-center bot-text-display bot-result">
					“The best way to predict the future is to invent it.” <br> – Alan Kay
				</div>
		    </div>
			<div class="row justify-content-center py-5">
				<div class="col-6 text-center">
					<strong>Spoken command: </strong><span id="current-command"></span>
				</div>
				<div class="col-6 text-center">
					<strong id="v-commands">voice commands</strong>&nbsp;
					<i class="commands-icon text-white fas fa-chevron-down"></i><br>
					<div class="v-commands-list">
						"remove/show list"<br>
						"turn on/off the light"<br>
						"light color [blue/red/green]"
					</div>
				</div>
		    </div>
		</div>
		


		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		<script src="./js/apex.js"></script>
	</body>
</html>